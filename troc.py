#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import simplekml

#Set filename/path for KML file output
kmlfile = "troc.kml"
#Set KML schema name
kmlschemaname = "troc"
#Set page URL
pageURL = "https://www.troc.com/fr/magasins"
#Start a session with the given page URL
session = requests.Session()
page = session.get(pageURL)
#Soupify the HTML
soup = BeautifulSoup(page.content, 'html.parser')
#Specify the card div class to get all the store information
stores = soup(class_="js-card-liste-magasin")

#Initialize kml object
kml = simplekml.Kml()
#Add schema, which is required for a custom address field
schema = kml.newschema(name=kmlschemaname)
schema.newsimplefield(name="address",type="string")

#Iterate through datamap for each store
for store in stores:
    #Pass a dictionary to find_all function for special HTML5 "data-*" attribute, and assign the store ID number (row[2]) as the value
    lat = store.get('data-lat')
    lng = store.get('data-lng')
    #Get the name of the store and prepend it with "Troc " to clearly indicate that this point is a Troc store on maps that contain other data sources
    storename = "Troc " + store.find(class_="name-magasin").a.get_text()
    #Get the address of the store, stripping all new lines and using a space to join the address lines
    storeaddress = store.find(class_="address").a.get_text(" ",strip=True)
    #Remove line breaks from the address
    #First, create the point name and description in the kml
    point = kml.newpoint(name=storename,description="thrift store")
    #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
    point.extendeddata.schemadata.schemaurl = kmlschemaname
    simpledata = point.extendeddata.schemadata.newsimpledata("address",storeaddress)
    #Finally, add coordinates to the feature
    point.coords=[(lng,lat)]
#Save the final KML file
kml.save(kmlfile)
