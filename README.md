# Troc Location Extractor
Extracts Troc (French second hand / thrift store) locations from the official website, and places them into KML format.

## Dependencies
* Python 3.x
    * Requests module for session functionality
    * Beautiful Soup 4.x (bs4) for scraping
    * Simplekml for easily building KML files
* Also of course depends on official [Troc website](https://www.troc.com/fr/magasins).
